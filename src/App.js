import React from 'react';
import CountriesApp from "./containers/CountriesApp/CountriesApp";

const App = () => {
    return (
        <>
          <CountriesApp/>
        </>
    );
};

export default App;