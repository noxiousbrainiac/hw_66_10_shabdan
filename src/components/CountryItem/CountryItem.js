import React from 'react';

const CountryItem = ({onItem, name}) => {
    return (
        <p
            style={{cursor: "pointer"}}
            onClick={onItem}
            className="m-1"
        >
            {name}
        </p>
    );
};

export default CountryItem;