import React from 'react';
import CountryItem from "../CountryItem/CountryItem";

const CountriesList = ({countries}) => {
    return (
        <div className="d-flex flex-column">
            {countries.map(item => (
                <CountryItem
                    key={item.name}
                    name={item.name}
                />
            ))}
        </div>
    );
};

export default CountriesList;