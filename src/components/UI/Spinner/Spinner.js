import React from 'react';
import './Spinner.css';
import Backdrop from "../Backdrop/Backdrop";

const Spinner = ({show}) => (
    <>
        <Backdrop show={show}/>
        <div
            className="Spinner"
            style={{
                display: show ? 'block' : 'none'
            }}
        >
            Loading...
        </div>
    </>

);

export default Spinner;