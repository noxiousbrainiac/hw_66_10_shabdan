import React, {useEffect, useMemo, useState} from 'react';
import Spinner from "../UI/Spinner/Spinner";
import Modal from "../UI/Modal/Modal";
import AxiosApi from "../../axiosApi";

const withErrorHandler = (WrappedComponent) => ({...props}) => {
        const [error, setError] = useState(null);
        const [preloader,setPreloader] = useState(true);

        const requestInterceptor = () => {
            AxiosApi.interceptors.request.use(
                req => {
                    return req;
                },
                error => {
                    setError(error);
                    throw error;
                }
            )
        }

        const responseInterceptor = () => {
            AxiosApi.interceptors.response.use(
                res => {
                    setPreloader(false);
                    return res;
                },
                error => {
                    setPreloader(false);
                    setError(error);
                    throw error;
                }
            )
        }

        const requestIn = useMemo(() => {
            requestInterceptor();
        }, [])

        const responseIn = useMemo(() => {
            responseInterceptor();
        }, [])

        useEffect(() => {
            return () => {
                AxiosApi.interceptors.request.eject(requestIn);
                AxiosApi.interceptors.response.eject(responseIn);
            }
        },[requestIn, responseIn])

        return (
            <>
                <Modal show={Boolean(error)}>[You catch Error]</Modal>
                <Spinner show={preloader}/>
                <WrappedComponent {...props}/>
            </>
        )
};

export default withErrorHandler;