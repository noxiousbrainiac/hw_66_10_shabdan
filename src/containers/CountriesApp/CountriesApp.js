import React, {useState, useEffect} from 'react';
import AxiosApi from "../../axiosApi";
import CountriesList from "../../components/CountryList/CountryList";
import withErrorHandler from "../../components/hoc/withErrorHandler";

const CountriesApp = () => {
    const [countries, setCountries] = useState([]);

    const getAxios = async () => {
        const data = await AxiosApi.get('/');
        setCountries(data.data);
    }

    useEffect(() => {
        getAxios();
    }, []);

    return (
        <div className="container">
            <CountriesList countries={countries}/>
        </div>
    );
};

export default withErrorHandler(CountriesApp);