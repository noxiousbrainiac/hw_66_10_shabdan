import axios from "axios";

const AxiosApi = axios.create({
    baseURL: 'https://restcountries.eu/rest/v2/all?fields=name;alpha3Code'
})

export default AxiosApi;